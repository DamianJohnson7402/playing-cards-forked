// Playing Cards
// Damian Johnson

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	TWO = 2, // you forgot to assign numerical values
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum class Suit
{
	SPADES,
	HEARTS,
	DIAMONDS,
	CLUBS
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case Rank::TWO: cout << "TWO"; break;
	case Rank::THREE: cout << "THREE"; break;
	case Rank::FOUR: cout << "FOUR"; break;
	case Rank::FIVE: cout << "FIVE"; break;
	case Rank::SIX: cout << "SIX"; break;
	case Rank::SEVEN: cout << "SEVEN"; break;
	case Rank::EIGHT: cout << "EIGHT"; break;
	case Rank::NINE: cout << "NINE"; break;
	case Rank::TEN: cout << "TEN"; break;
	case Rank::JACK: cout << "JACK"; break;
	case Rank::QUEEN: cout << "QUEEN"; break;
	case Rank::KING: cout << "KING"; break;
	case Rank::ACE: cout << "ACE"; break;
	default: cout << "Invalid"; break;
	}

	cout << " of ";

	switch (card.suit)
	{
	case Suit::CLUBS: cout << "CLUBS"; break;
	case Suit::HEARTS: cout << "HEARTS"; break;
	case Suit::DIAMONDS: cout << "DIAMONDS"; break;
	case Suit::SPADES: cout << "SPADES"; break;
	default: cout << "Invalid"; break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
		return card1;
	else
		return card2;
}

int main()
{
	Card card1;
	Card card2;

	card1.rank = Rank::FOUR;
	card1.suit = Suit::HEARTS;

	card2.rank = Rank::JACK;
	card2.suit = Suit::SPADES;

	PrintCard(card1);
	cout << "\n";
	PrintCard(card2);
	cout << "\n\n";

	PrintCard(HighCard(card1, card2));
	cout << " is the HIGHER card!";

	(void)_getch();
	return 0;
}
